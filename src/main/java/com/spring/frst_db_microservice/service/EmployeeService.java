package com.spring.frst_db_microservice.service;

import com.spring.frst_db_microservice.exception.NoContentException;
import com.spring.frst_db_microservice.models.Employee;
import com.spring.frst_db_microservice.repository.EmployeeMongoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@AllArgsConstructor
public class EmployeeService {
    private EmployeeMongoRepository employeeMongoRepository;

    public List<Employee> getAllEmployee() {
        return employeeMongoRepository.findAll();
    }

    public Employee getEmployerByNameAndSurname(String name, String surname) {
        Optional<Employee> employeeOptional = employeeMongoRepository.getEmployeeByNameAndSurname(name, surname);

        return employeeOptional.orElseThrow(NoContentException::new);
    }

    public void createEmployee(String name, String surname, String post) {
        if(employeeMongoRepository.getEmployeeByNameAndSurname(name, surname).isEmpty()) {
            employeeMongoRepository.save(new Employee(null, name, surname, post));
        }
    }

    public void updateEmployee(String name, String surname, String newName, String newSurname, String newPost) {
        if(employeeMongoRepository.getEmployeeByNameAndSurname(name, surname).isPresent()) {
            Employee employee = getEmployerByNameAndSurname(name, surname);

            employee.setName(newName);
            employee.setSurname(newSurname);
            employee.setPost(newPost);

            employeeMongoRepository.save(employee);
        }
    }

    public void deleteEmployee(String name, String surname) {
        employeeMongoRepository.deleteByNameAndSurname(name, surname);
    }
}
