package com.spring.frst_db_microservice.controller;

import com.spring.frst_db_microservice.models.Employee;
import com.spring.frst_db_microservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping(value = "/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public List<Employee> getEmployeeListAsString() {
        return employeeService.getAllEmployee();
    }

    @RequestMapping(value = "/create", method = RequestMethod.PUT)
    public @ResponseBody
    void addEmployee(@RequestParam(name = "name") String name,
                     @RequestParam(name = "surname") String surname,
                     @RequestParam(name = "post") String post) {

        employeeService.createEmployee(name, surname, post);
    }

    @RequestMapping(value = "/read", method = RequestMethod.GET)
    public @ResponseBody
    Employee readEmployee(@RequestParam(name = "name") String name,
                          @RequestParam(name = "surname") String surname) {

        return employeeService.getEmployerByNameAndSurname(name, surname);
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public @ResponseBody
    void updateEmployee(@RequestParam(name = "name") String name,
                        @RequestParam(name = "surname") String surname,
                        @RequestParam(name = "newName") String newName,
                        @RequestParam(name = "newSurname") String newSurname,
                        @RequestParam(name = "newPost") String newPost) {

        employeeService.updateEmployee(name, surname, newName, newSurname, newPost);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public @ResponseBody
    void deleteEmployee(@RequestParam(name = "name") String name,
                        @RequestParam(name = "surname") String surname) {

        employeeService.deleteEmployee(name, surname);
    }
}
