package com.spring.frst_db_microservice.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(value = "employee")
public  class Employee {

    @Id
    private String id;
    private String name;
    private String surname;
    private String post;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public Employee() {

    }

    public Employee(String id, String name, String surname, String post) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.post = post;
    }
}
