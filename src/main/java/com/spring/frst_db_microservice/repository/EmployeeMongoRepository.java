package com.spring.frst_db_microservice.repository;

import com.spring.frst_db_microservice.models.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface EmployeeMongoRepository extends MongoRepository<Employee, String> {
    Optional<Employee> getEmployeeByNameAndSurname(String name, String surname);

    void deleteByNameAndSurname(String name, String surname);
}
