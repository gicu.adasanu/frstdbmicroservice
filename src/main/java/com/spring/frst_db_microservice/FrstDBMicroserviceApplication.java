package com.spring.frst_db_microservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrstDBMicroserviceApplication {

	public static void main(String[] args) {

		SpringApplication.run(FrstDBMicroserviceApplication.class, args);
	}
}
